# cpanel_sslcheckall

A tool that walks through all sites configured in cPanel and validates the installed certificate and prints any CAA records that were found for the given domain. 